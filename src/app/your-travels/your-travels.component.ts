import { Component, OnInit } from '@angular/core';
import { Travel } from '../travel';

@Component({
  selector: 'app-your-travels',
  templateUrl: './your-travels.component.html',
  styleUrls: ['./your-travels.component.css']
})
export class YourTravelsComponent implements OnInit {

  travels: Travel[] = [];
  travelModel: Travel;

  constructor() {
    this.travels.push(new Travel(1, 'Polska', 'Warszawa', '22-10-2019', '25-10-2019', '5000'));
    this.travels.push(new Travel(2, 'Hawaje', 'Honolulu', '22-10-2019', '25-10-2019', '5000'));
    this.travels.push(new Travel(3, 'Kuba', 'Hawana', '22-10-2019', '25-10-2019', '5000'));
   }

  ngOnInit() {
  }

  onEdit(index: number) {

  }

  onDelete(index: number) {
    this.travels.splice(index, 1);
  }

  openDetails(index: number) {
  }


}
