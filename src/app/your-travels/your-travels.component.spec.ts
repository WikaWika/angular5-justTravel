import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourTravelsComponent } from './your-travels.component';

describe('YourTravelsComponent', () => {
  let component: YourTravelsComponent;
  let fixture: ComponentFixture<YourTravelsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourTravelsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourTravelsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
