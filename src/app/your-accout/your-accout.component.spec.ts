import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourAccoutComponent } from './your-accout.component';

describe('YourAccoutComponent', () => {
  let component: YourAccoutComponent;
  let fixture: ComponentFixture<YourAccoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourAccoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourAccoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
