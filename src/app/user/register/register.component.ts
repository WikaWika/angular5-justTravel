import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  onSubmit(name, lastname, city, datebirth, email, passwordOne, passwordTwo) {
    console.log(name, lastname, city, datebirth, email, passwordOne, passwordTwo);
    this.userService.userRegister(name, lastname, city, datebirth, email, passwordOne, passwordTwo);
  }

}
