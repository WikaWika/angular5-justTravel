export class Travel {


  constructor( public id: number,
    public country: string,
    public city: string,
    public dateStart: string,
    public dateEnd: string,
    public cost: string) {}
}
