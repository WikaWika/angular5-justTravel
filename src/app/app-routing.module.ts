import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { YourTravelsComponent } from './your-travels/your-travels.component';
import { LoginComponent } from './login/login.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { RegisterComponent } from './user/register/register.component';
import { YourAccoutComponent } from './your-accout/your-accout.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'yourtravels', component : YourTravelsComponent },
  { path: 'login', component : LoginComponent },
  { path: 'sign-in', component : SignInComponent },
  { path: 'sign-up', component : RegisterComponent },
  { path: 'youraccount', component : YourAccoutComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
