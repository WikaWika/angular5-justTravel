import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { enableProdMode} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { YourTravelsComponent } from './your-travels/your-travels.component';
import { AppRoutingModule } from './app-routing.module';
import { ModalAddTravelComponent } from './modal-add-travel/modal-add-travel.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { RegisterComponent } from './user/register/register.component';
import { YourAccoutComponent } from './your-accout/your-accout.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    YourTravelsComponent,
    ModalAddTravelComponent,
    LoginComponent,
    UserComponent,
    SignInComponent,
    RegisterComponent,
    YourAccoutComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    FormsModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
