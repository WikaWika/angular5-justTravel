import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddTravelComponent } from './modal-add-travel.component';

describe('ModalAddTravelComponent', () => {
  let component: ModalAddTravelComponent;
  let fixture: ComponentFixture<ModalAddTravelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddTravelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddTravelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
