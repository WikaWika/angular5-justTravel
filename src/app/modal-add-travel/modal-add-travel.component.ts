import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef  } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators, FormsModule } from '@angular/forms';
import { Travel } from '../travel';
import { ReactiveFormsModule } from '@angular/forms';
import { ElementDef } from '@angular/core/src/view';
import { YourTravelsComponent} from '../your-travels/your-travels.component';

@Component({
  selector: 'app-modal-add-travel',
  templateUrl: './modal-add-travel.component.html',
  styleUrls: ['./modal-add-travel.component.css']
})
export class ModalAddTravelComponent implements OnInit {

  isSubmitted: boolean = false;
  yourTravelsComponent: YourTravelsComponent;
  
  travelModel: Travel;
  closeResult: string;
  myform: FormGroup;

  country: FormControl; 
  city: FormControl;
  startDate: FormControl;
  endDate: FormControl;
  cost: FormControl


  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  createFormControls() { 
    this.country = new FormControl('', Validators.required);
    this.city = new FormControl('', Validators.required);
    this.startDate = new FormControl('', Validators.required);
    this.endDate = new FormControl('', Validators.required);
    this.cost = new FormControl('', Validators.required);
  }

  createForm() { 
    this.myform = new FormGroup({
     country: this.country,
     city: this.city,
     startDate: this.startDate,
     endDate: this.endDate,
     cost: this.cost

    });
  }

  constructor(private modalService: NgbModal, private fb: FormBuilder) {}



  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.myform.reset();
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
}

  onSubmit() {
    if (this.myform.valid) {
      console.log("Form Submitted!");
      this.myform.reset();

      this.yourTravelsComponent.travels.push(this.travelModel);
    }
    else{
      console.log("dupa!");
    }
  }

}
